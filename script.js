$(function () {
  console.log("best zoom module init"); 
  getPosts("https://banco-best.pt/feed/wp-json/bbdmkwm/v1/zoom-posts/1").done(
    function (data) {
      addContent(data);
    }
  );
});

function getPosts(url) {
  return $.ajax({
    url: url,
    dataType: "json",
  });
}

function addContent(data) {
  Array.prototype.forEach.call(data, function (el, i) {
    var html = "";

    if (parseInt(i) > 3) {
      return;
    }

    idPost = el.ID;
    title = el.post_title ? el.post_title : "";
    postText = el.post_content ? el.post_content.substr(0, 232) + "..." : "";
    date = el.date ? el.date.split("-") : "";
    date = date[1];
    month = el.month_year ? el.month_year.split(" ") : "";
    month = month[0].substr(0, 3) + "";
    date = date + "&nbsp" + month;

    author = el.author_name ? el.author_name : "";
    authorimg = el.author_img ? el.author_img : "";
    img = el.image ? el.image : "";
    if (parseInt(i) > 0 && i < 3) {
      html += '<div class="col-6">';
      html +=
        '<a href="https://www.bancobest.pt/ptg/best_zoom?post=' + idPost + '">';

      html += '		<div class="bbest-zoom-news-item-image">';
      html += '				<img src="' + img + '" alt="News Title"/>';
      html += "		</div>";
      html += '		<div class="bbest-zoom-news-lead">';
      html +=
        '						<div class="bbest-zoom-news-author"> <h4 class="bbest-zoom-news-text-md">' +
        author +
        "&nbsp&nbsp|</div>";
      html +=
        '						<div class="bbest-zoom-date"><h4 class="bbest-zoom-news-text-md">&nbsp ' +
        date +
        " </h4></div>";
      html += "		</div>";
      html += '		<div class="bbest-zoom-news-title">';
      html += "				<h2>" + title + "</h2>";
      html += "		</div>";
      html +=
        '<span class="icon-container"><img  class="icon" alt="icon-arrow" src="bestsite/best_microsite/mkt_files/paginas-detalhe/images-21/icon/icon-arrow.svg"><img  class="shadow" alt="icon-arrow" src="bestsite/best_microsite/mkt_files/paginas-detalhe/images-21/icon/icon-arrow.svg"></span>';
      html += "</a>";

      html += "</div>";
      $(".bbest-zoom-another-news").append(html);
    } else if (parseInt(i) < 1) {
      html +=
        '<a href="https://www.bancobest.pt/ptg/best_zoom?post=' + idPost + '">';
      html += '		<div class="bbest-zoom-news-item-image">';
      html += '				<img src="' + img + '" alt="News Title"/>';
      html += "		</div>";
      html += '		<div class="bbest-zoom-news-lead">';
      html +=
        '						<div class="bbest-zoom-news-author"><h4 class="bbest-zoom-news-text-md">' +
        author +
        "&nbsp|</h4></div>";
      html +=
        '						<div class="bbest-zoom-date"><h4 class="bbest-zoom-news-text-md">&nbsp' +
        date +
        " </h4></div>";
      html += "		</div>";

      html +=
        '		<div class="bbest-zoom-news-title"> <h2>' + title + "</h2></div>";
      html += '		<div class="bestzoom-news-text">';
      html +=
        '		<div class="bestzoom-first-news-content">  <h4 class="bbest-zoom-news-text-md">' +
        postText +
        "</h4></div>";
      html += "		</div>";

      html +=
        '<span class="icon-container"><img  class="icon" alt="icon-arrow" src="bestsite/best_microsite/mkt_files/paginas-detalhe/images-21/icon/icon-arrow.svg"><img  class="shadow" alt="icon-arrow" src="bestsite/best_microsite/mkt_files/paginas-detalhe/images-21/icon/icon-arrow.svg"></span>';

      html += "		</a>";

      $(".bbest-zoom-last-news").append(html);
      $(".bbest-zoom-last-news").attr(
        "href",
        "https://www.bancobest.pt/ptg/best_zoom?post=" + idPost
      );
    }
  });
}

